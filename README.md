To Download: Click the cloud-looking button on the right of the screen

To launch game:
- Download or Pull folder Guichet_ATM_ReleaseBuild.
- Launch GreyJack.exe

To view solution:
- Download or Pull folder Guichet_ATM_Solution.
- Open GreyJack.sln in your IDE or double click it.
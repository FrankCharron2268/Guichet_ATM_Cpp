#include <iostream>
#include <math.h> // pour modulo de double fmod(a,b)
#include <fstream> //pour write read .txt
#include <iomanip>
#include <string>
#include <conio.h>
#include <windows.h> //pour manipuler la console
#include <cstdlib> //pour exit()

using namespace std;

struct strCompte {
	string Nom;
	string NoAcc;
	string Nip;
	string Solde;
	strCompte *Suivant;
};

struct strList
{
	strCompte *Premier;
	strCompte *Dernier;
	int Total = 0;
};
#pragma region Prototypes
void setFont();
#pragma region Prototypes Fonctions Visuelles
void displayInterface();///affiche un interface dynamiquement, loading screen, fera appel a loadSimulation() alternance avec sa propre execution
void EraseInterface();///efface dynamiquement linterface et affiche le loading screen
int loadSimulation(string, int);///affiche le compteur loading screen
void showTitle();///affichera le les elements fixes de linterface
void setCursorXY(int, int); ///place le curseur
#pragma endregion
#pragma region Prototypes Fonctions Interactives
void readFile(strCompte*, strList*);///extrait donn�es fichier texte, appel la fonction createAccs()
void createAccs(strCompte*, strList*, string[], int);///cr�e liste chain�es des comptes a laide du data receuillit avec readFile()
void connectAcc(strCompte*, strList*); ///1ere interaction,demande nip et login afin de receuillir le compte de la liste chainee, simule un loading screen displayIntertface() avant dappeler showMenu()
void createNewAcc(strCompte*, strList*);
void showMenu(strCompte*);///2iem user interaction appel�e a partir de connectAcc() on affiche le menu et propose 4 interactions
void makeDeposit(strCompte*);///selon le choix du user et effectue la transaction
void makeWithdraw(strCompte*);///selon le choix du user on effectue la transaction
void showInfo(strCompte*);///selon le choix du user on effectue la transaction
void exitProgram();///quitte le programme et fait appel
void writetoFile(strCompte*);
#pragma endregion
#pragma endregion

#pragma region Fonctions

void setFont()
{
	::SendMessage(::GetConsoleWindow(), WM_SYSKEYDOWN, VK_RETURN, 0x20000000);/// Met la console en FullScreen
	setlocale(LC_ALL, "");
	HANDLE outcon = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_FONT_INFOEX font;
	GetCurrentConsoleFontEx(outcon, false, &font);
	SetConsoleTextAttribute(outcon, 0x002); ///couleure du texte
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = 0;
	cfi.dwFontSize.Y = 24;                 ///set la taille du texte
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;
	std::wcscpy(cfi.FaceName, L"Consolas"); // Choose your font
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}
int loadSimulation(string message, int cnt)
{
	for (double i = 1; i < 5; i++)
	{
		setCursorXY(78, 15);
		Sleep(5);
		if (cnt * 3 > 100)
		{
			cout << message << 100;
		}
		else
		{
			cout << message << cnt * 3;
			Sleep(1);
			cnt++;
		}
	}
	if (cnt == 10)
	{
		cnt = 0;
	}
	cout << endl;
	return cnt;
}
void displayInterface()
{
	char logoTop1[] = { "$===============================//=============================//" };

	char logoTop2[] = { " //        Banque Royale        //" };

	char logoTop3[] = { "  //=============================//===============================$" };

	char logoBot1[] = { "$===============================//=============================//  " };
	char logoBot2[] = { "//        Banque Royale        //" };
	char logoBot3[] = { "//=============================//===============================$" };

	string SideL = "{|";
	string SideR = "|}";
	int cnt = 0;
	cnt = loadSimulation("Connexion........", cnt);

	for (int a = 0; a < strlen(logoTop1); a++)
	{
		Sleep(1);
		setCursorXY(40 + a, 3);
		cout << logoTop1[a];
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int b = strlen(logoTop2); b > 0; b--)
	{
		Sleep(1);
		setCursorXY(72 + b, 2);
		cout << logoTop2[b];
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int c = 0; c < strlen(logoTop3); c++)
	{
		Sleep(1);
		setCursorXY(72 + c, 1);

		cout << logoTop3[c];
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int d = 0; d < 35; d++)
	{
		Sleep(1);
		setCursorXY(138, 2 + d);
		cout << SideR;
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int e = 0; e < strlen(logoBot1); e++)
	{
		Sleep(1);
		setCursorXY(138 - e, 37);
		cout << logoBot1[e];
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int f = 0; f < strlen(logoBot2); f++)
	{
		Sleep(1);
		setCursorXY(73 + f, 38);
		cout << logoBot2[f];
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int g = 0; g < strlen(logoBot3); g++)
	{
		Sleep(1);
		setCursorXY(104 - g, 39);
		cout << logoBot3[g];
	}
	cnt = loadSimulation("Connexion........", cnt);
	for (int h = 0; h < 36; h++)
	{
		Sleep(1);
		setCursorXY(39, 39 - h);
		cout << SideL;
	}
	cnt = loadSimulation("Connexion........", cnt);
}
void EraseInterface()
{
	char logoTop1[] = { "$===============================//=============================//" };

	char logoTop2[] = { " //        Banque Royale        //" };

	char logoTop3[] = { "  //=============================//===============================$" };

	char logoBot1[] = { "$===============================//=============================//  " };
	char logoBot2[] = { "//       Guichet Atm           //" };
	char logoBot3[] = { "//=============================//===============================$" };
	int cnt = 0;
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int a = 0; a < strlen(logoTop1); a++)
	{
		Sleep(1);
		setCursorXY(40 + a, 3);
		cout << " ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int b = 0; b < strlen(logoTop2); b++)
	{
		Sleep(1);
		setCursorXY(72 + b, 2);
		cout << " ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int c = 0; c < strlen(logoTop3); c++)
	{
		Sleep(1);
		setCursorXY(72 + c, 1);

		cout << " ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int d = 0; d < 35; d++)
	{
		Sleep(1);
		setCursorXY(138, 2 + d);
		cout << "  ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int e = 0; e < strlen(logoBot1); e++)
	{
		Sleep(1);
		setCursorXY(138 - e, 37);
		cout << " ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int f = strlen(logoBot2); f >= 0; f--)
	{
		Sleep(1);
		setCursorXY(73 + f, 38);
		cout << " ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int g = 0; g < strlen(logoBot3); g++)
	{
		Sleep(1);
		setCursorXY(104 - g, 39);
		cout << " ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
	for (int h = 0; h < 36; h++)
	{
		Sleep(1);
		setCursorXY(39, 39 - h);
		cout << "  ";
	}
	cnt = loadSimulation("Deconnexion........", cnt);
}
void setCursorXY(int x, int y)
{								      //=====================================================//
									  //Fonction place le curseur selon les parametres recus //
									  //=====================================================//
	COORD p = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}
///Fonctions au dessus sont destin�es uniquement a laffichage de linterface
void showTitle()
{
	char infoLogs[] = {
		"  Comptes de base  "
		"__________________"
		"||  Login| Nip  ||"
		"||  +++++|++++  ||"
		"||  C001 | C001 ||"
		"||  C002 | C002 ||"
		"||  C003 | C003 ||"
		"||  C004 | C004 ||"
		"||  C005 | C005 ||"
		"|| admin | admin||"
		"+++------------+++"
	};
	int cnt = 0;
	for (int i = 0; i < 11; i++) // double for qui affichera mon tableau de char tel que presenter ci haut
	{
		for (int j = 0; j < 18; j++)
		{
			setCursorXY(2 + j, 2 + i);
			cout << infoLogs[cnt];
			++cnt;
		}
	}
	char logoTop1[] = { "$===============================//=============================//" };
	char logoTop2[] = { " //        Banque Royale        //" };
	char logoTop3[] = { "  //=============================//===============================$" };

	char logoBot1[] = { "$===============================//=============================//  " };
	char logoBot2[] = { "//        Banque Royale        //" };
	char logoBot3[] = { "//=============================//===============================$" };
	///jaurais pu facilement me servir de seulement 3 logo[] mais je men suis rendu compte rendu au For G
	string SideL = "{|";
	string SideR = "|}";

	for (int a = 0; a < strlen(logoTop1); a++)
	{
		setCursorXY(40 + a, 3);
		cout << logoTop1[a];
	}

	for (int b = strlen(logoTop2); b > 0; b--)
	{
		setCursorXY(72 + b, 2);
		cout << logoTop2[b];
	}

	for (int c = 0; c < strlen(logoTop3); c++)
	{
		setCursorXY(72 + c, 1);

		cout << logoTop3[c];
	}

	for (int d = 0; d < 35; d++)
	{
		setCursorXY(138, 2 + d);
		cout << SideR;
	}

	for (int e = 0; e < strlen(logoBot1); e++)
	{
		setCursorXY(138 - e, 37);
		cout << logoBot1[e];
	}

	for (int f = 0; f < strlen(logoBot2); f++)
	{
		setCursorXY(73 + f, 38);
		cout << logoBot2[f];
	}

	for (int g = 0; g < strlen(logoBot3); g++)
	{
		setCursorXY(104 - g, 39);
		cout << logoBot3[g];
	}

	for (int h = 0; h < 36; h++)
	{
		setCursorXY(39, 39 - h);
		cout << SideL;
	}
	setCursorXY(80, 38);
	cout << "GUICHET AUTOMATIQUE" << endl;
	setCursorXY(0, 0);
	cout << "Appuyez sur F11 pour quitter le mode Plein Ecran" << endl;
}
void readFile(strCompte *ptrCompte, strList *maListe)
{
	string line;
	ifstream myfile("data.txt");

	if (myfile.is_open())
	{
		int cnt = 0;

		while (getline(myfile, line))
		{
			cnt++;
		}
		ifstream myfile("data.txt");
		int i = 0;
		string *myArray;
		myArray = new string[cnt];

		while (getline(myfile, line))
		{
			myArray[i] = line;
			i++;
		}

		myfile.close();
		createAccs(ptrCompte, maListe, myArray, cnt);
	}

	else cout << "Unable to open file";
}
void createAccs(strCompte *ptrCompte, strList *maListe, string myArray[], int cnt)
{
	for (int i = 0; i < cnt; i += 4)
	{
		ptrCompte = new strCompte;
		ptrCompte->Nom = myArray[i];
		ptrCompte->NoAcc = myArray[i + 1];
		ptrCompte->Nip = myArray[i + 2];
		ptrCompte->Solde = myArray[i + 3];
		ptrCompte->Suivant = NULL;
		(maListe->Total == 0) ? maListe->Premier = ptrCompte : maListe->Dernier->Suivant = ptrCompte;

		maListe->Dernier = ptrCompte;
		maListe->Total++;
	}
}
void connectAcc(strCompte *ptrCompte, strList *maListe)
{
	string Login;
	string Password;

	int i = 0;

	string choix = "n";
	do {
		system("cls");
		showTitle();
		setCursorXY(76, 13);
		cout << "Entrez Numero de Compte : ";
		cin >> Login;
		setCursorXY(76, 14);
		cout << "Entrez le mot de passe  : ";
		cin >> Password;
		for (ptrCompte = maListe->Premier; ptrCompte != NULL; ptrCompte = ptrCompte->Suivant)
		{
			if (ptrCompte->NoAcc == Login && ptrCompte->Nip == Password)
			{
				system("cls");
				displayInterface();
				showTitle();
				setCursorXY(78, 13);

				cout << "Bienvenu Mr. " << ptrCompte->Nom;
				setCursorXY(80, 14);
				showMenu(ptrCompte);
				goto endfunction;
			}
			else if (Login == "admin" && Password == "admin")
			{
				createNewAcc(ptrCompte, maListe);
				goto endfunction;
			}
			i++;
		}
		system("cls");
		showTitle();
		setCursorXY(78, 13);
		cout << "Login ou Mot de passe invalide";
		setCursorXY(78, 14);
		cout << "Desirez vous reesayer? y/n : ";
		cin >> choix;
	} while (choix != "n" || choix != "N");
endfunction:;
}
void createNewAcc(strCompte *ptrCompte, strList *maListe)
{
	system("cls");
	displayInterface();
	system("cls");
	showTitle();
	ptrCompte = new strCompte;
	setCursorXY(70, 13);
	cout << "Entrez le nom du client              : ";
	cin >> ptrCompte->Nom;
	setCursorXY(70, 14);
	cout << "Entrez le numero de compte client  : ";
	cin >> ptrCompte->NoAcc;
	setCursorXY(70, 15);
	cout << "Entrez le mot de passe du client   : ";
	cin >> ptrCompte->Nip;
	setCursorXY(70, 16);
	cout << "Entrez le montant du depot initial : ";
	cin >> ptrCompte->Solde;
	setCursorXY(70, 17);
	ptrCompte->Suivant = NULL;
	(maListe->Total == 0) ? maListe->Premier = ptrCompte : maListe->Dernier->Suivant = ptrCompte;

	maListe->Dernier = ptrCompte;
	maListe->Total++;
}
void showMenu(strCompte *ptrCompte)
{
	system("cls");
	showTitle();
	int Choix = 0;
	setCursorXY(68, 13);
	cout << "Quelle Operation desirez vous effectuer, Mr." << ptrCompte->Nom << "?" << endl;

	string Menu[] = {  /// Simplification de la technique que jai utiliser pour afficher la boite logins / nips.
		" ___________________ ",
		"| 1- Pour Retirer   |",
		"| 2- pour Deposer   |",
		"| 3- Pour Consulter |",
		"| 4- Pour Terminer  |",
		"*-------------------*",
		"     Operation? : "
	};
	for (int i = 0; i < 7; i++)
	{
		if (i != 6)
		{
			setCursorXY(79, 14 + i);
			cout << Menu[i] << endl;
		}
		else
		{
			setCursorXY(79, 14 + i);
			cout << Menu[i];
		}
	}
	cin.ignore();
	cin >> Choix;
	switch (Choix) {
	case 1:
		makeWithdraw(ptrCompte);
		break;
	case 2:
		makeDeposit(ptrCompte);
		break;
	case 3:
		showInfo(ptrCompte);
		break;
	case 4:
		exitProgram();
		break;
	}
}
void makeDeposit(strCompte *ptrCompte)
{
	double Depot = 20;
	system("cls");
	showTitle();

	string temp = ptrCompte->Solde;
	double tmpSolde = ::atof(temp.c_str());
	do {
		setCursorXY(65, 14);
		cout << "Max = 20000$, Min=20$, Multiples de 20$ Seulement";
		setCursorXY(75, 15);
		cout << "                                   ";
		setCursorXY(75, 15);
		cout << "Entrez le montant a deposer : ";
		cin.ignore();
		cin >> Depot;
	} while (Depot < 20 || Depot > 20000 || fmod(Depot, 20) != 0);
	tmpSolde += Depot;
	system("cls");
	showTitle();
	setCursorXY(72, 15);
	cout << "Votre nouveau solde est de : " << tmpSolde << "$";
	string Solde = to_string(tmpSolde);
	ptrCompte->Solde = Solde;
	setCursorXY(66, 18);
	wcout << "Appuyez sur une touche, pour retourner au menu";
	system("pause>nul");
	showMenu(ptrCompte);
}
void makeWithdraw(strCompte *ptrCompte)
{
	double Retrait = 20;
	system("cls");
	showTitle();

	string temp = ptrCompte->Solde;
	double tmpSolde = ::atof(temp.c_str());
	do {
		setCursorXY(65, 14);
		cout << "Max = 500$, Min=20$, Multiples de 20$ Seulement";
		setCursorXY(75, 15);
		cout << "                                   ";
		setCursorXY(75, 15);
		cout << "Entrez le montant a retirer : ";
		cin.ignore();
		cin >> Retrait;
	} while (Retrait > tmpSolde || Retrait < 20 || Retrait > 500 || fmod(Retrait, 20) != 0);
	tmpSolde -= Retrait;
	system("cls");
	showTitle();
	setCursorXY(72, 15);
	cout << "Votre nouveau solde est de : " << tmpSolde << "$";
	string Solde = to_string(tmpSolde);
	ptrCompte->Solde = Solde;
	setCursorXY(66, 18);
	wcout << "Appuyez sur une touche, pour retourner au menu";
	system("pause>nul");
	showMenu(ptrCompte);
}
void showInfo(strCompte *ptrCompte)
{
	system("cls");
	showTitle();
	setCursorXY(75, 14);
	cout << "Nom              : " << ptrCompte->Nom;
	setCursorXY(75, 15);
	cout << "Solde            : " << ptrCompte->Solde << "$";
	setCursorXY(75, 16);
	cout << "Numero de compte : " << ptrCompte->NoAcc;
	setCursorXY(75, 17);
	cout << "Nip              : " << ptrCompte->Nip;
	setCursorXY(66, 18);
	wcout << "Appuyez sur une touche, pour retourner au menu";
	system("pause>nul");
	showMenu(ptrCompte);
}
void exitProgram()
{
	system("cls");
	showTitle();
	EraseInterface();
}

void writetoFile(strCompte *ptrCompte, strList *maListe)
{
	ofstream myfile("data.txt");

	if (myfile.is_open())
	{
		for (ptrCompte = maListe->Premier; ptrCompte != NULL; ptrCompte = ptrCompte->Suivant)
		{
			myfile << ptrCompte->Nom << "\n" << ptrCompte->NoAcc << "\n" << ptrCompte->Nip << "\n" << ptrCompte->Solde << "\n";
		}
		myfile.close();
	}
}
#pragma endregion

void main()
{
	setFont();///customisation de la console taille,couleur texte/fenetre

			  ///initialisation de mes structures
	strList *maListe = new strList;
	strCompte *ptrCompte;

	ptrCompte = new strCompte;

	char choix = ' ';
#pragma region Deroulement principal
	do {
		readFile(ptrCompte, maListe);/// Extraction des donn�es du fichier texte

		showTitle();	///affiche les elements fixes de linterface
		connectAcc(ptrCompte, maListe);///connexion a laide de login et password,
									   //les appels des fonctions suivantes seront effectu�es a linterieur de la fonction precedente
		system("cls");
		showTitle();

		setCursorXY(67, 14); ///positionne le curseur
		cout << "Desirez vous etablir une nouvelle connexion? y/n : ";
		cin >> choix;

		writetoFile(ptrCompte, maListe);
	} while (choix != 'n' || choix != 'N' && choix == 'y' || choix == 'Y');
#pragma endregion

	setCursorXY(69, 14);
	cout << "Merci de faire confiance a la Banque Royale!   " << endl;
	for (int i = 0; i < 6; i++)
	{
		system("cls");
		setCursorXY(69, 15);
		cout << "------------------Au Revoir-----------------";
		Sleep(500);
		setCursorXY(60, 16);
	}

	Sleep(1500);
}